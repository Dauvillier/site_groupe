//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//Footer + titre Page Voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};


//header Titre barre nav
var Titre = 'Les Petites Pastèques en vacances';

//Milieu
var Article1 = {
	title : "Allemagne",
	texte : "L’Allemagne, on en parle pour son économie, sa Chancelière, son histoire, sa musique, ses sportifs et notamment la Mannschaft qui a gagné le trophée le plus convoité de la sphère footballistique. Mais c’est également un pays qui regorge de lieux somptueux et de villes multiculturelles, alternatives et très différentes les unes des autres.",
	image : "http://www.luxmundi-voyages.com/wp-content/uploads/2016/03/pont-dheidelberg-700x300.jpg"

};

var Article2 = {
	title : "Croatie",
	texte : "La Croatie est variée en découvertes ! Il est indispensable de visiter ses îles, participer à ses différents festivals, aller au parc national des lacs de Plitvice, mais aussi de se promener dans Dubrovnik !",
	image : "https://www.eurolines.de/fileadmin/content/bus_international/Laender/eurolines_bus_kroatien.jpg"

};

var Article3 = {
	title : "Espagne",
	texte : "L’Espagne, pays du soleil, de la fête et de la gastronomie ne pourra que vous conquérir. Eté comme hiver, il y aura toujours un endroit en Espagne où vous pourrez profiter du soleil et de la mer. On peut aussi se laisser tenter par des cours de flamenco !",
	image : "https://www.eurolines.de/fileadmin/content/bus_international/Laender/eurolines_bus_spanien.jpg"

};

var Article4 = {
	title : "Grèce",
	texte : "Des vacances en Grèce seront l’occasion de lever le voile sur les origines de la civilisation occidentale. La capitale, Athènes, regorge de trésors historiques. La mythologie imprègne le moindre recoin du pays : sur le mont Olympe, l’Acropole ou le Parthénon",
	image : "https://media.shoko.fr/pmedia-3435238-ajust_700-f5513559/coucher-de-soleil-sur-les-fameuses-maisons.jpg"

};


//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';


$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('#Destinations').append(menu[1]);
	


	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);


	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Allemagne').append(Article1.title);
	$('#TexteAllemagne').append(Article1.texte);

	$('#Croatie').append(Article2.title);
	$('#TexteCroatie').append(Article2.texte);

	$('#Espagne').append(Article3.title);
	$('#TexteEspagne').append(Article3.texte);

	$('#Grece').append(Article4.title);
	$('#TexteGrece').append(Article4.texte);


	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton);
	$('#Bouton2').append(Bouton);
	$('#Bouton3').append(Bouton);

	//Effet bouton quans souris passe
	$('#Bouton').mouseenter(function(){
		$('#Bouton').fadeOut().fadeIn();
	});

	//Effet bouton quans souris passe
	$('#Bouton1').mouseenter(function(){
		$('#Bouton1').fadeOut().fadeIn();
	});

	//Effet bouton quans souris passe
	$('#Bouton2').mouseenter(function(){
		$('#Bouton2').fadeOut().fadeIn();
	});

	//Effet bouton quans souris passe
	$('#Bouton3').mouseenter(function(){
		$('#Bouton3').fadeOut().fadeIn();
	});



});