//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Destinations - Thaïlande';

//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';


//Milieu
var Article1 = {
	title : "Parc National Dio Inthanon",
	texte : "Mis à part le sommet de la montagne et les fleurs magnifiques qu’il abrite, il y a bien sur un nombre incalculable d’autres choses à voir dans le parc.",
	image : "https://render.fineartamerica.com/images/rendered/search/print/images/artworkimages/medium/1/twin-pagoda-in-doi-inthanon-national-park-anek-suwannaphoom.jpg"

};

var Article2 = {
	title : "Temple Wat Rong Khun",
	texte : "À la fin du 20eme siècle, le Wat Rong Khun original, était un vieux temple qui tombait en ruine. Un artiste thaïlandais de renom, Chalermchai Kositpipat, a alors proposé de le rénover totalement pour rendre hommage au roi Rama IX et à Chiang Rai, sa ville natale.",
	image : "https://www.mifuguemiraison.com/wp-content/uploads/2017/03/DSC_1645-300x200.jpg"

};

var Article3 = {
	title : "Ancient Siam",
	texte : "Le parc Ancient City (Muang Boran) regroupe en un seul lieu, des reproductions des plus beaux monuments de Thaïlande ainsi que des villages typiques, marché flottant, temples, palais…",
	image : "https://image.jimcdn.com/app/cms/image/transf/none/path/s3cf902bfb8ba11b3/image/i83582bf8030e4eaf/version/1450476766/image.jpg"

};


//afficher titre voyage
$('title').html(SiteConfig.title);
console.log(SiteConfig);

//footer
$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('.Destinations').append(menu[0]);
	$('#SousTitre').append(SousTitre);

	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Doi').append(Article1.title);
	$('#TexteDoi').append(Article1.texte);

	$('#Temple').append(Article2.title);
	$('#TexteTemple').append(Article2.texte);

	$('#Siam').append(Article3.title);
	$('#TexteSiam').append(Article3.texte);

	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton1);
	$('#Bouton2').append(Bouton2);

	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);


	$('.img1').mouseov
	function bigImg(x) {
		x.style.height = "64px";
		x.style.width = "64px";
	}

	function normalImg(x) {
		x.style.height = "32px";
		x.style.width = "32px";
	}

//Effet flou + fleche quand souris passe sur img
	$('#Effet').ready(function(){
		$('#Image2').mouseout(function(){
		$('#Image2').hide();
		$('#Image3').hide();
		return false;
		});

		$('#Image1').mouseenter(function(){
		$('#Image2').show();
		$('#Image3').show();
		return false;
		});

	});

	$('#Effet1').ready(function(){
		$('#Image22').mouseout(function(){
		$('#Image22').hide();
		$('#Image33').hide();
		return false;
		});

		$('#Image11').mouseenter(function(){
		$('#Image22').show();
		$('#Image33').show();
		return false;
		});

	});
	
	$('#Effet11').ready(function(){
		$('#Image222').mouseout(function(){
		$('#Image222').hide();
		$('#Image333').hide();
		return false;
		});

		$('#Image111').mouseenter(function(){
		$('#Image222').show();
		$('#Image333').show();
		return false;
		});

	});
	
});

