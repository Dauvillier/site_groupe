//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Destinations - Croatie';

//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';



//Milieu
var Article1 = {
	title : "Arboretum de Tresteno",
	texte : "Situé à une vingtaine de kilomètre au Nord-Ouest de Dubrovnik en longeant la mer (face à l’île de Lopud), Trestno est un village qui abrite un très bel arboretum fondé par la famille noble Gučetić au 15ème siècle",
	image : "http://a401.idata.over-blog.com/300x200/2/72/62/85/CROATIE/4-CAVTAT.jpg"

};

var Article2 = {
	title : "Dubrovnik",
	texte : "La vielle ville de Dubrovnik en elle-même est incontournable, dotée d’un riche patrimoine culturel et historique, important et surtout, elle offre la possibilité de se baigner aux pieds des fortifications",
	image : "http://images.salaun-holidays.com/(Vignette)-vignette-Croatie-Dubrovnik-5-2017.jpg"

};

var Article3 = {
	title : "Parc National des Lacs de Plitvice",
	texte : "Ce grand Parc National, inscrit au Patrimoine Mondial de l'Unesco, englobe une succession de 16 lacs dont la figuration est tout à fait particulière.",
	image : "https://img.ev.mu/images/villes/21571/300x200/21571.jpg"

};


//afficher titre voyage
$('title').html(SiteConfig.title);
console.log(SiteConfig);

//footer
$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('.Destinations').append(menu[0]);
	$('#SousTitre').append(SousTitre);

	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Arboretum').append(Article1.title);
	$('#TexteArboretum').append(Article1.texte);

	$('#Ville').append(Article2.title);
	$('#TexteVille').append(Article2.texte);

	$('#Parc').append(Article3.title);
	$('#TexteParc').append(Article3.texte);

	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton1);
	$('#Bouton2').append(Bouton2);

	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);


	$('.img1').mouseov
	function bigImg(x) {
		x.style.height = "64px";
		x.style.width = "64px";
	}

	function normalImg(x) {
		x.style.height = "32px";
		x.style.width = "32px";
	}
//Effet flou + fleche quand souris passe sur img
	$('#Effet').ready(function(){
		$('#Image2').mouseout(function(){
		$('#Image2').hide();
		$('#Image3').hide();
		return false;
		});

		$('#Image1').mouseenter(function(){
		$('#Image2').show();
		$('#Image3').show();
		return false;
		});

	});

	$('#Effet1').ready(function(){
		$('#Image22').mouseout(function(){
		$('#Image22').hide();
		$('#Image33').hide();
		return false;
		});

		$('#Image11').mouseenter(function(){
		$('#Image22').show();
		$('#Image33').show();
		return false;
		});

	});
	
	$('#Effet11').ready(function(){
		$('#Image222').mouseout(function(){
		$('#Image222').hide();
		$('#Image333').hide();
		return false;
		});

		$('#Image111').mouseenter(function(){
		$('#Image222').show();
		$('#Image333').show();
		return false;
		});

	});
	
});

