//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Destinations - Grèce';

//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';


//Milieu
var Article1 = {
	title : "Acropole d'Athènes",
	texte : "Symbole de la civilisation grecque antique, l'Acropole, qui date du 5e siècle avant J-C, est l'un des sites plus visités au monde. Inscrite au patrimoine mondial de l'Unesco depuis 1987, cette colline réunit plusieurs monuments dont le fameux Parthénon",
	image : "https://images.salaun-holidays.com/(vignette)-Vignette-Grece-Athenes-Acropole-Parthenon-04-it_42616900-09032017.jpg"

};

var Article2 = {
	title : "Monastères des Météores",
	texte : "Ils sont situés dans la préfecture de Thessalie en Grèce centrale et c’est vraiment un endroit à la beauté singulière.C’est également l’un des complexes religieux les plus importants du pays. ",
	image : "https://www.mifuguemiraison.com/wp-content/uploads/2016/04/DSC06382-e1462828315854-300x200.jpg"

};

var Article3 = {
	title : "Lindos",
	texte : "Lindos est un petit village où l’on peut trouver de nombreuses activités et pleins de choses à voir. Le village est vraiment pittoresque avec ses maisons blanches, deux magnifiques plages à égale distance du village, et un site archéologique majeur, l’Acropole de Lindos.",
	image : "http://www.greek-tourism.com/island_images/rhodes2.jpg"

};


//afficher titre voyage
$('title').html(SiteConfig.title);
console.log(SiteConfig);

//footer
$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('.Destinations').append(menu[0]);
	$('#SousTitre').append(SousTitre);

	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Acropole').append(Article1.title);
	$('#TexteAcropole').append(Article1.texte);

	$('#Meteores').append(Article2.title);
	$('#TexteMeteores').append(Article2.texte);

	$('#Lindos').append(Article3.title);
	$('#TexteLindos').append(Article3.texte);

	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton1);
	$('#Bouton2').append(Bouton2);

	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);


	$('.img1').mouseov
	function bigImg(x) {
		x.style.height = "64px";
		x.style.width = "64px";
	}

	function normalImg(x) {
		x.style.height = "32px";
		x.style.width = "32px";
	}

//Effet flou + fleche quand souris passe sur img
	$('#Effet').ready(function(){
		$('#Image2').mouseout(function(){
		$('#Image2').hide();
		$('#Image3').hide();
		return false;
		});

		$('#Image1').mouseenter(function(){
		$('#Image2').show();
		$('#Image3').show();
		return false;
		});

	});

	$('#Effet1').ready(function(){
		$('#Image22').mouseout(function(){
		$('#Image22').hide();
		$('#Image33').hide();
		return false;
		});

		$('#Image11').mouseenter(function(){
		$('#Image22').show();
		$('#Image33').show();
		return false;
		});

	});
	
	$('#Effet11').ready(function(){
		$('#Image222').mouseout(function(){
		$('#Image222').hide();
		$('#Image333').hide();
		return false;
		});

		$('#Image111').mouseenter(function(){
		$('#Image222').show();
		$('#Image333').show();
		return false;
		});

	});
	
	
});

