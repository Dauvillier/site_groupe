//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Destinations - Espagne';

//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';

//Milieu
var Article1 = {
	title : "Sagrada Familia",
	texte : "La Sagrada Familia est l'une des oeuvres les plus connues de Antoni Gaudí dans Barcelone. C'est une Basilique gigantesque dont la construction a débuté en 1882. ",
	image : "http://www.mediatheque.cat/envolees/wp-content/uploads/2016/03/Sagrada-Familia_Antoni-Gaudi_dezeen_936_0-300x200.jpg"

};

var Article2 = {
	title : "Alhambra",
	texte : "Son nom vient de l’Arabe, “la Rouge” car sa pierre devient rouge au coucher du soleil. Il s’agit également du seul palais Arabe construit au Moyen Âge (1238) qui soit encore intact. Avec 4 entrées et autant de sorties, il n’est pas facile de tout voir de ce lieu magique en une seule visite.",
	image : "https://crm48.com/wp-content/uploads/2017/06/29934568_s-300x200.jpg"

};

var Article3 = {
	title : "Casa Batllo",
	texte : "Que l'on nomme également « La Casa de los huesos » (maison des os en espagnole) pour l’originalité de son architecture, dans laquelle grands nombres d’éléments nous rappellent le squelette humain, la Casa Batllo est l’un des chefs-d’oeuvre de la ville de Barcelone dont l'architecte Antoni Gaudi en est l’auteur.",
	image : "http://i.f1g.fr/media/ext/300x200/www.lefigaro.fr/medias/2013/06/25/PHOed30dde2-dd87-11e2-bd90-b865394bf7b0-300x200.jpg"

};


//afficher titre voyage
$('title').html(SiteConfig.title);
console.log(SiteConfig);

//footer
$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('.Destinations').append(menu[0]);
	$('#SousTitre').append(SousTitre);

	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Sagrada').append(Article1.title);
	$('#TexteSagrada').append(Article1.texte);

	$('#Alhambra').append(Article2.title);
	$('#TexteAlhambra').append(Article2.texte);

	$('#Casa').append(Article3.title);
	$('#TexteCasa').append(Article3.texte);

	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton1);
	$('#Bouton2').append(Bouton2);

	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);


	$('.img1').mouseov
	function bigImg(x) {
		x.style.height = "64px";
		x.style.width = "64px";
	}

	function normalImg(x) {
		x.style.height = "32px";
		x.style.width = "32px";
	}

	//Effet flou + fleche quand souris passe sur img
	$('#Effet').ready(function(){
		$('#Image2').mouseout(function(){
		$('#Image2').hide();
		$('#Image3').hide();
		return false;
		});

		$('#Image1').mouseenter(function(){
		$('#Image2').show();
		$('#Image3').show();
		return false;
		});

	});

	$('#Effet1').ready(function(){
		$('#Image22').mouseout(function(){
		$('#Image22').hide();
		$('#Image33').hide();
		return false;
		});

		$('#Image11').mouseenter(function(){
		$('#Image22').show();
		$('#Image33').show();
		return false;
		});

	});
	
	$('#Effet11').ready(function(){
		$('#Image222').mouseout(function(){
		$('#Image222').hide();
		$('#Image333').hide();
		return false;
		});

		$('#Image111').mouseenter(function(){
		$('#Image222').show();
		$('#Image333').show();
		return false;
		});

	});
});

