//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Destinations - Allemagne';

//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';


//Milieu
var Article1 = {
	title : "Lac de Constance",
	texte : "Sur l'île de Lindau, on se promène au bord du lac de Constance en profitant de la vue sur l'entrée du port. Il est gardé par une statue en grès de 6 mètres représentant un lion assis, symbole de la Bavière, et par un ancien phare de 37 mètres de haut.",
	image : "https://img.ev.mu/images/villes/1864/300x200/1864.jpg"

};

var Article2 = {
	title : "Porte de Brandebourg",
	texte : "Aucun lieu n'a joué un rôle aussi important dans l'histoire mouvementée de Berlin que la Porte de Brandebourg. C'est ici que des étapes importantes de l'histoire ont été écrites : le cortège triomphal de Napoléon, les défilés nazis et les discours sinistres d'Hitler, un no man's land pendant la guerre froide, la visite de JFK,...",
	image : "http://www.quandpartir.com/meteo/images/villes/allemagne-berlin-300.jpg"

};

var Article3 = {
	title : "Palais de Zwinger",
	texte : "Dresde, charmante ville allemande, abrite l’une des pépites historiques nationales : le palais de Zwinger. Autrefois lieu de festivités et de relaxation pour les souverains de Saxe, ce bâtiment est l’un des plus visités d’Allemagne.",
	image : "https://thumbs.dreamstime.com/thumb_587/5879379.jpg"

};


//afficher titre voyage
$('title').html(SiteConfig.title);
console.log(SiteConfig);

//footer
$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('.Destinations').append(menu[0]);
	$('#SousTitre').append(SousTitre);

	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Lac').append(Article1.title);
	$('#TexteLac').append(Article1.texte);

	$('#Porte').append(Article2.title);
	$('#TextePorte').append(Article2.texte);

	$('#Palais').append(Article3.title);
	$('#TextePalais').append(Article3.texte);

	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton1);
	$('#Bouton2').append(Bouton2);

	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);

	$('.img1').mouseov
	function bigImg(x) {
		x.style.height = "64px";
		x.style.width = "64px";
	}

	function normalImg(x) {
		x.style.height = "32px";
		x.style.width = "32px";
	}

	//Effet flou + fleche quand souris passe sur img
	$('#Effet').ready(function(){
		$('#Image2').mouseout(function(){
		$('#Image2').hide();
		$('#Image3').hide();
		return false;
		});

		$('#Image1').mouseenter(function(){
		$('#Image2').show();
		$('#Image3').show();
		return false;
		});

	});

	$('#Effet1').ready(function(){
		$('#Image22').mouseout(function(){
		$('#Image22').hide();
		$('#Image33').hide();
		return false;
		});

		$('#Image11').mouseenter(function(){
		$('#Image22').show();
		$('#Image33').show();
		return false;
		});

	});
	
	$('#Effet11').ready(function(){
		$('#Image222').mouseout(function(){
		$('#Image222').hide();
		$('#Image333').hide();
		return false;
		});

		$('#Image111').mouseenter(function(){
		$('#Image222').show();
		$('#Image333').show();
		return false;
		});

	});
});

