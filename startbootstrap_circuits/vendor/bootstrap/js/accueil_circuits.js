//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Circuits",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Circuits';


//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';
var Bouton3 = 'Par ici !';
var Bouton4 = 'Par ici !';
var Bouton5 = 'Par ici !';
var Bouton6 = 'Par ici !';


//milieu : les circuits ordre alpha 
var Article1 = {
	title : "Autriche",
	texte : "Cet itinéraire très complet vous fera découvrir les principales cités historiques du pays, comme Innsbruck, Salzbourg, Vienne et Graz. Immergez-vous dans le Vienne impérial, marchez sur les traces de Mozart et découvrez le folklore autrichien.",
	image : "https://static5.evcdn.net/images/reduction/136825_w-700_h-400_q-50_m-crop.jpg"

};

var Article2 = {
	title : "Canada",
	texte : "De la région de Québec en passant par Montréal et les Chutes du Niagara, venez découvrir les richesses culturelles de l Est du Canada, ainsi que son environnement naturel époustouflant. Un circuit au Canada extraordinaire qui vous laissera un souvenir impérissable. Survol des Chutes du Niagara en hélicoptère inclus. Montréal, Québec, le Lac St-Jean, Ottawa, Les Chutes du Niagara, Toronto De la région de Québec en passant par Montréal et les Chutes.",
	image : "http://yashdestinations.com/imagestours/rockymountain2.gif"

};

var Article3 = {
	title : "Cambodge",
	texte : "Le célèbre fleuve du Mékong est le fil conducteur de ce voyage dans le temps... Vous verrez se succèder la ville de Phnom Penh, les temples d’Angkor, le site du Preah Vihear inscrit au Patrimoine Mondial de l’UNESCO, et les temples pré-angkoriens du Wat Phou, situés au sud du Laos et entourés de rizières. Vous découvrirez aussi la capitale Vientiane et ses monuments, le charme discret des ruelles de Luang Prabang, riche de superbes temples et de restaurants réputés.",
	image : "http://www.purnouvellezelande.com/wp-content/uploads/2013/05/voyage_nouvelle_zelande24.jpg"

};

var Article4 = {
	title : "Espagne",
	texte : "Découvrez les villes d Andalousie chargées d histoire, les sites classés au patrimoine mondial de l UNESCO. Laissez-vous envoûter par les couleurs, la musique et l ambiance festive du sud de l Espagne. Partez à la découverte de villes dont les seuls noms évoquent l histoire exceptionnellement riche de l Andalousie...Séville, Cordoue Grenade héâtre de beautés architecturales uniques et paysages magnifiques, cette terre du sud de l Espagne noyée de soleil est enivrante",
	image : "http://madrideando.es/wp-content/uploads/2017/02/malaga.jpg"

};

var Article5 = {
	title : "Finlande",
	texte : "Un séjour multi-activités, de 8 jours, très varié à faire en famille ou entre amis, qui ravira les petits et les grands. Au programme : chien de traîneau, motoneige, randonnée sur le parc naturel de Auttiköngas ou Korouoma, rencontre avec les rennes, et bien sûr, la visite du village du père Noel et du musée de l arctique et visite du zoo de Ranua.",
	image : "http://www.groupeactis.com/wp-content/uploads/2017/06/Finlande.jpg"

};

var Article6 = {
	title : "Nouvelle Zélande",
	texte : "Ce circuit en français de 15 jours d Auckland à Christchurch offre une découverte complète de la Nouvelle-Zélande, et privilégie les petits groupes en limitant à 10 participants maximum. Tous les hauts lieux touristiques de l île du Nord et de l île du Sud sont au programme. Bénéficiez de toute l expertise et les connaissances de votre chauffeur guide. Partez en toute tranquillité à la découverte des paysages grandioses de la Nouvelle Zélande, destination qui semble avoir été créée pour les amoureux de la nature.",
	image : "http://www.aoravoyages.com/wp-content/uploads/2014/12/iStock_000045867626_Medium-700x400.jpg"

};


$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	for (var i = 0; i < menu.length; i++) {
		$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		}

	$('.navbar-brand').append(Titre);
	$('#SousTitre').append(SousTitre);

	

	//Afficher le contenu du milieu (liste des circuits) (image, titre et texte)
	$('#Circuit1').append(Article1.title);
	$('#TexteCircuit1').append(Article1.texte);

	$('#Circuit2').append(Article2.title);
	$('#TexteCircuit2').append(Article2.texte);

	$('#Circuit3').append(Article3.title);
	$('#TexteCircuit3').append(Article3.texte);

	$('#Circuit4').append(Article4.title);
	$('#TexteCircuit4').append(Article4.texte);

	$('#Circuit5').append(Article5.title);
	$('#TexteCircuit5').append(Article5.texte);

	$('#Circuit6').append(Article6.title);
	$('#TexteCircuit6').append(Article6.texte);
	
	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);

//fonction cacher  texte 1
	$('#TexteJour1').ready(function(){
		$('#TexteJour1').hide();
	});
	$('#Img1').click(function(){
		$('#TexteJour1').show();
	});

	//fonction cacher texte 2
	$('#TexteJour2').ready(function(){
		$('#TexteJour2').hide();
	});
	$('#Img2').click(function(){
		$('#TexteJour2').show();
	});
	
	//fonction cacher texte 3
	$('#TexteJour3').ready(function(){
		$('#TexteJour3').hide();
	});
	$('#Img3').click(function(){
		$('#TexteJour3').show();
	});

	//fonction cacher texte 4
	$('#TexteJour4').ready(function(){
		$('#TexteJour4').hide();
	});
	$('#Img4').click(function(){
		$('#TexteJour4').show();
	});


	//fonction image caché derrière

	
	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton);
	$('#Bouton2').append(Bouton);
	$('#Bouton3').append(Bouton);
	$('#Bouton4').append(Bouton);
	$('#Bouton5').append(Bouton);
	$('#Bouton6').append(Bouton);

	//Changer couleur bouton quans souris passe
	$('#Bouton').mouseenter(function(){
		$('#Bouton').fadeOut().fadeIn();
	});

	//Changer couleur bouton quans souris passe
	$('#Bouton1').mouseenter(function(){
		$('#Bouton1').fadeOut().fadeIn();
	});

	//Changer couleur bouton quans souris passe
	$('#Bouton2').mouseenter(function(){
		$('#Bouton2').fadeOut().fadeIn();
	});

	//Changer couleur bouton quans souris passe
	$('#Bouton3').mouseenter(function(){
		$('#Bouton3').fadeOut().fadeIn();
	});

	//Changer couleur bouton quans souris passe
	$('#Bouton4').mouseenter(function(){
		$('#Bouton4').fadeOut().fadeIn();
	});

	//Changer couleur bouton quans souris passe
	$('#Bouton5').mouseenter(function(){
		$('#Bouton5').fadeOut().fadeIn();
	});

//Changer couleur bouton quans souris passe
	$('#Bouton6').mouseenter(function(){
		$('#Bouton6').fadeOut().fadeIn();
	});
});


//changer couleur texte quand souris passe
 $('#Circuit1').mouseover(function(){
		$('#Circuit1').hover();
		});

