//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Circuits - Nouvelle Zélande",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Circuits - Nouvelle Zélande';

//Titre du circuit
var Description = 'Mosaïque Néo-Zélandaise';

//Détails du circuit
var Details = 'Ce circuit en français de 15 jours d Auckland à Christchurch offre une découverte complète de la Nouvelle-Zélande, et privilégie les petits groupes en limitant à 10 participants maximum. Tous les hauts lieux touristiques de l île du Nord et de l île du Sud sont au programme. Bénéficiez de toute l expertise et les connaissances de votre chauffeur guide. Partez en toute tranquillité à la découverte des paysages grandioses de la Nouvelle Zélande, destination qui semble avoir été créée pour les amoureux de la nature.';

//Liste des principaux thèmes du voyage
var Liste1 = 'Nature';
var Liste2 = 'Paysages';
var Liste3 = 'Randonnées';
var Liste4 = 'Dégustations';

//Titre des articles
var Programme = 'Le programme';

//milieu les articles du programme 
var Article1 = {
	title : "Jour 1 - Arrivée à Auckland",
	texte : "Auckland est la plus grande ville de Nouvelle-Zélande, située entre deux grandes baies, dotées de cratères de volcans éteints depuis bien longtemps. C'est aussi le carrefour de plusieurs cultures, sans oublier évidemment les All Blacks et l'America's Cup ! Connue aussi sous le nom de “City Sails” (la cité des voiles), on ne peut manquer les centaines de voiliers glissant au gré du vent dans la magnifique baie d Auckland !",
	image : "https://www.croisieres-exception.fr/var/actualites/large-1453-crystal-aircruises-lance-sa-toute-premiere-croisiere-en-avion.jpg"

};

var Article2 = {
	title : "Jour 2 - Franz Joseph – Queenstown",
	texte : "Avant d'arriver à Queenstown, vous ferez un arrêt à Arrowtown, la ville de la ruée vers l'or la mieux préservée de la région. Arrivée en fin d'après-midi à Queenstown. Fantastiquement située entre lac et montagne, la ville est bien connue pour ses sports extrêmes et expériences scéniques mais aussi pour sa gastronomie et son vin, les croisières sur le lac, le shopping et le golf ! Dîner et nuit à l hôtel Heartland en chambre Standard.",
	image : "https://www.travelcounsellors.com.au/media/2931409/nzfranzjosef.jpg?width=500&height=300"

};

var Article3 = {
	title : "Jour 3 - Péninsule de Coromandel – Rotorua",
	texte : "Après le petit-déjeuner, direction le Nord de Coromandel où vous pourrez vous prélasser à Hot Water Beach. Eau chaude naturelle dont les bulles viennent de la terre profonde, vous pourrez choisir votre propre bain à remous sur la plage (si la marée le permet) et admirer juste en face de vous l'océan ! Vous poursuivrez votre route jusqu'à Hahei Beach. Passage par Te Pare, réserve historique, appartenant à un ancien Maori Pa (village fortifié).",
	image : "http://www.purnouvellezelande.com/wp-content/uploads/2013/05/voyage_nouvelle_zelande24.jpg"

};

var Article4 = {
	title : "Jour 4 - Tongariro National Park",
	texte : "Après le petit-déjeuner, départ pour la capitale de Nouvelle-Zélande : Wellington, petite ville colorée et pleine de charme, située entre collines et baies. Tour de ville de Wellington sans manquer la vue panoramique du Mont Victoria. Vous pourrez flâner en centre-ville et vous promener dans les rues animées, bordées de magasins et de cafés, ou faire une visite libre du musée national de Te Papa. Dîner libre et nuit au Quality Hotel en Hôtel Suite.",
	image : "http://www.johnb.co.nz/images/Lower%20Tama%20Lake.jpg"

};



$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	for (var i = 0; i < menu.length; i++) {
		$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		}

	$('.navbar-brand').append(Titre);
	$('#SousTitre').append(SousTitre);
	$('#Description').append(Description);

	//Afficher le paragrpahe explicatif du circuit
	$('#Details').append(Details);

	//Afficher les listes
	$('#Liste1').append(Liste1);
	$('#Liste2').append(Liste2);
	$('#Liste3').append(Liste3);
	$('#Liste4').append(Liste4);

	//Afficher le programme
	$('#Programme').append(Programme);


	//Afficher le contenu du milieu du programme (image, titre et texte)
	$('#Jour1').append(Article1.title);
	$('#TexteJour1').append(Article1.texte);

	$('#Jour2').append(Article2.title);
	$('#TexteJour2').append(Article2.texte);

	$('#Jour3').append(Article3.title);
	$('#TexteJour3').append(Article3.texte);

	$('#Jour4').append(Article4.title);
	$('#TexteJour4').append(Article4.texte);
	
	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);


//fonction cacher  texte 1
	$('#TexteJour1').ready(function(){
		$('#TexteJour1').hide();
	});
	$('#Img1').click(function(){
		$('#TexteJour1').show();
	});


	//fonction cacher texte 2
	$('#TexteJour2').ready(function(){
		$('#TexteJour2').hide();
	});
	$('#Img2').click(function(){
		$('#TexteJour2').show();
	});
	
	//fonction cacher texte 3
	$('#TexteJour3').ready(function(){
		$('#TexteJour3').hide();
	});
	$('#Img3').click(function(){
		$('#TexteJour3').show();
	});

	//fonction cacher texte 4
	$('#TexteJour4').ready(function(){
		$('#TexteJour4').hide();
	});
	$('#Img4').click(function(){
		$('#TexteJour4').show();
	});

	

	//fonction afficher grande image  cachée derrière image
	$('#GrandeImage2').ready(function(){
			$('#GrandeImage2').hide();
	});

	$('#GrandeImage2').mouseover(function(){
		$('#GrandeImage1').show();
		$("#GrandeImage2").hide();
	});

	 $('#GrandeImage1').mouseout(function(){
		$("#GrandeImage1").hide();
		$('#GrandeImage2').show();
	});

});



//fonction montrer l'image Copie derrière l'image 1 (passer curseur dessus)
	 $('#Img1Copie').mouseover(function(){
		$('#Img1').show();
		$("#Img1Copie").hide();
	});

	 $('#Img1').mouseout(function(){
		$("#Img1").hide();
		$('#Img1Copie').show();
	});

//fonction montrer l'image Copie derrière l'image 2 (passer curseur dessus)
	 $('#Img2Copie').mouseover(function(){
		$('#Img2').show();
		$("#Img2Copie").hide();
	});

	 $('#Img2').mouseout(function(){
		$("#Img2").hide();
		$('#Img2Copie').show();
	});


	 //fonction montrer l'image Copie derrière l'image 3 (passer curseur dessus)
	 $('#Img3Copie').mouseover(function(){
		$('#Img3').show();
		$("#Img3Copie").hide();
	});

	 $('#Img3').mouseout(function(){
		$("#Img3").hide();
		$('#Img3Copie').show();
	});


 //fonction montrer l'image Copie derrière l'image 4 (passer curseur dessus)
	 $('#Img4Copie').mouseover(function(){
		$('#Img4').show();
		$("#Img4Copie").hide();
	});

	 $('#Img4').mouseout(function(){
		$("#Img4").hide();
		$('#Img4Copie').show();
	});

  	