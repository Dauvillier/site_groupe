//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Circuits - Cambodge",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Circuits - Cambodge';

//Titre du circuit
var Description = 'Cambodge - Siem Reap';

//Détails du circuit
var Details = ' Le célèbre fleuve du Mékong est le fil conducteur de ce voyage dans le temps... Vous verrez se succèder la ville de Phnom Penh, les temples d’Angkor, le site du Preah Vihear inscrit au Patrimoine Mondial de l’UNESCO, et les temples pré-angkoriens du Wat Phou, situés au sud du Laos et entourés de rizières. Vous découvrirez aussi la capitale Vientiane et ses monuments, le charme discret des ruelles de Luang Prabang, riche de superbes temples et de restaurants réputés.';

//Liste des principaux thèmes du voyage
var Liste1 = 'Temples';
var Liste2 = 'Traditions' ;
var Liste3 = 'Spécialités culinaires';
var Liste4 = 'Monuments';

//Titre des articles
var Programme = 'Le programme';

//milieu les articles du programme 
var Article1 = {
	title : "Jour 1 - Siem Reap",
	texte : "Départ de votre hôtel pour atteindre un petit village authentique cerné de rizières et de cultures maraîchères. Visite à pied pour découvrir les produits locaux du marché et rencontrer les vanniers qui font la renommée de ce petit village. Départ en char à boeufs sur les pistes à travers les cultures d’aubergine, de concombre amer et autres cultures prisées des Cambodgiens (1h de charrette). Retour au village pour une présentation et la préparation de votre déjeuner, avec le fameux amok , une salade aux fleurs de bananes, et le som low kor kour", 
	image : "https://i.pinimg.com/originals/2b/f5/03/2bf50305cebb3469129e6dfa106f0e8f.jpg"

};

var Article2 = {
	title : "Jour 2 - Angkor",
	texte : "Vous visiterez le temple Banteay Srei, en grès rose finement sculpté. Déjeuner, dégustation du amok . L'après-midi, visite de la Porte sud d’Angkor Thom, le Bayon et ses 49 tours aux 200 visages, le Baphûon, temple de forme pyramidale représentant le mythique mont Meru, le temple Phimeanakas, les terrasses aux éléphants et du roi lépreux. Dîner de spécialités : le porc au caramel kor sach chhrouk et spectacle de danses traditionnelles khmères.",
	image : "http://www.lafrancaisedescircuits.biz/images/photo/B_363_iStock_000063299705_web.jpg"

};

var Article3 = {
	title : "Jour 3 - Battambang",
	texte : "Visitez le petit musée renfermant une collection de linteaux et de statues, la ville, son marché coloré, la Maison du gouverneur (vue extérieure), les maisons de négoce françaises, une maison traditionnelle khmère, la pagode Slaket et le temple hindou Ek Phnom dédié à Shiva. Profitez de votre soirée pour flâner...",
	image : "https://ip1.orchestra-platform.com/sI7l4HTWh-cJGxQq2NToLEBP5qkhEhdT_Vpue-QPzmFw/http://www.lafrancaisedescircuits.biz/images/photo/B_239_KH_PalaisRoyalPhnomPenh155960192.jpg"

};

var Article4 = {
	title : "Jour 4 - Phnom Penh",
	texte : "Visite de la capitale : le Musée national, le palais Royal et sa pagode d’Argent qui renferme de superbes collections et le Wat Phnom (le sanctuaire le plus ancien de la ville). Visite du marché central et de l’ancienne prison S-21, transformée en musée du Génocide. Transfert à l’aéroport de Phnom Penh. ( prévoir un vol décollage le soir de Phnom Penh, non inclus).",
	image : "http://www.lafrancaisedescircuits.biz/images/photo/B_243_AT_HALLSTATT_111896819.jpg"

};



$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	for (var i = 0; i < menu.length; i++) {
		$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		}

	$('.navbar-brand').append(Titre);
	$('#SousTitre').append(SousTitre);
	$('#Description').append(Description);

	//Afficher le paragrpahe explicatif du circuit
	$('#Details').append(Details);

	//Afficher les listes
	$('#Liste1').append(Liste1);
	$('#Liste2').append(Liste2);
	$('#Liste3').append(Liste3);
	$('#Liste4').append(Liste4);

	//Afficher le programme
	$('#Programme').append(Programme);


	//Afficher le contenu du milieu du programme (image, titre et texte)
	$('#Jour1').append(Article1.title);
	$('#TexteJour1').append(Article1.texte);

	$('#Jour2').append(Article2.title);
	$('#TexteJour2').append(Article2.texte);

	$('#Jour3').append(Article3.title);
	$('#TexteJour3').append(Article3.texte);

	$('#Jour4').append(Article4.title);
	$('#TexteJour4').append(Article4.texte);
	
	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);



//fonction cacher texte texte 1
	$('#TexteJour1').ready(function(){
		$('#TexteJour1').hide();
	});
	$('#Img1').click(function(){
		$('#TexteJour1').show();
	});

	//fonction cacher texte texte 2
	$('#TexteJour2').ready(function(){
		$('#TexteJour2').hide();
	});
	$('#Img2').click(function(){
		$('#TexteJour2').show();
	});
	
	//fonction cacher texte texte 3
	$('#TexteJour3').ready(function(){
		$('#TexteJour3').hide();
	});
	$('#Img3').click(function(){
		$('#TexteJour3').show();
	});

	//fonction cacher texte texte 4
	$('#TexteJour4').ready(function(){
		$('#TexteJour4').hide();
	});
	$('#Img4').click(function(){
		$('#TexteJour4').show();
	});

});

//fonction afficher grande image  cachée derrière image
	$('#GrandeImage2').ready(function(){
			$('#GrandeImage2').hide();
	});

	$('#GrandeImage2').mouseover(function(){
		$('#GrandeImage1').show();
		$("#GrandeImage2").hide();
	});

	 $('#GrandeImage1').mouseout(function(){
		$("#GrandeImage1").hide();
		$('#GrandeImage2').show();
	});



//fonction montrer l'image Copie derrière l'image 1 (passer curseur dessus)
	 $('#Img1Copie').mouseover(function(){
		$('#Img1').show();
		$("#Img1Copie").hide();
	});

	 $('#Img1').mouseout(function(){
		$("#Img1").hide();
		$('#Img1Copie').show();
	});

//fonction montrer l'image Copie derrière l'image 2 (passer curseur dessus)
	 $('#Img2Copie').mouseover(function(){
		$('#Img2').show();
		$("#Img2Copie").hide();
	});

	 $('#Img2').mouseout(function(){
		$("#Img2").hide();
		$('#Img2Copie').show();
	});


	 //fonction montrer l'image Copie derrière l'image 3 (passer curseur dessus)
	 $('#Img3Copie').mouseover(function(){
		$('#Img3').show();
		$("#Img3Copie").hide();
	});

	 $('#Img3').mouseout(function(){
		$("#Img3").hide();
		$('#Img3Copie').show();
	});


 //fonction montrer l'image Copie derrière l'image 4 (passer curseur dessus)
	 $('#Img4Copie').mouseover(function(){
		$('#Img4').show();
		$("#Img4Copie").hide();
	});

	 $('#Img4').mouseout(function(){
		$("#Img4").hide();
		$('#Img4Copie').show();
	});
