//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Circuits - Espagne",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Circuits - Espagne';

//Titre du circuit
var Description = 'Espagne - Andalousie';

//Détails du circuit
var Details = 'Découvrez les villes d Andalousie chargées d histoire, les sites classés au patrimoine mondial de l UNESCO. Laissez-vous envoûter par les couleurs, la musique et l ambiance festive du sud de l Espagne. Partez à la découverte de villes dont les seuls noms évoquent l histoire exceptionnellement riche de l Andalousie...Séville, Cordoue Grenade héâtre de beautés architecturales uniques et paysages magnifiques, cette terre du sud de l Espagne noyée de soleil est enivrante';

//Liste des principaux thèmes du voyage
var Liste1 = 'Patrimoine';
var Liste2 = 'Paysages';
var Liste3 = 'Monuments';
var Liste4 = 'Architecture';

//Titre des articles
var Programme = 'Le programme';

//milieu les articles du programme 
var Article1 = {
	title : "Jour 1 - Ronda, Séville",
	texte : "Départ pour Ronda, cité historique et berceau de l'art tauromachique. Visite d'un élevage de taureaux de combat et de chevaux, symbole de l'Andalousie. Puis temps libre dans la ville pour découvrir le vieux pont, les arènes, le musée du Bandolero (la vie des brigands les plus célèbres), ou le centre historique. Déjeuner à Ronda. Puis continuation vers Séville, installation et dîner à l'hôtel.", 
	image : "https://ip1.orchestra-platform.com/sRJCZNG0L1n3ahN2-GIEh27XL2jo1WBiRKJJO-JalM6o/http://www.lafrancaisedescircuits.biz/images/photo/B_257_ES_Grenade_Alhambra_101163622.jpg"

};

var Article2 = {
	title : "Jour 2 - Cordoue",
	texte : "Départ de Séville, arrêt à Carmona, joli village blanc. À Cordoue, installation et déjeuner à l'hôtel. Découverte guidée : la Mezquita, la mosquée aujourd'hui cathédrale, chef-d'oeuvre de l'art islamique ; l'ancien quartier juif de la Juderia, son labyrinthe de ruelles aux façades blanchies, ses patios fleuris, les plus anciens de la ville et les statues des grands hommes tel Maimonide.",
	image : "https://ip1.orchestra-platform.com/sqnq2o_K_-EjtQEkSlM4fRNEX5MkO2qoJG56xzxasQcQ/http://www.lafrancaisedescircuits.biz/images/photo/B_440_ES_Cordoue-528627558.jpg"

};

var Article3 = {
	title : "Jour 3 - Malaga",
	texte : "Arrêt à Nerja, nommée le balcon de l'Europe par le roi Alfonso XII d'Espagne. Entrées aux grottes, les plus visitées du pays. Déjeuner avec apéritif et tapas. Continuation vers Malaga, tour panoramique. Installation à l'hôtel sur la Costa del Sol, Torremolinos ou station proche. Dîner.",
	image : "http://www.lafrancaisedescircuits.biz/images/photo/B_440_ThinkstockPhotos-524153337.jpg"

};

var Article4 = {
	title : "Jour 4 - Grenade",
	texte : "Arrêt à Baena ou Luque pour voir un moulin à huile et découvrir la fabrication de l or jaune andalous. Continuation vers Grenade, au pied de la Sierra Nevada, dernière ville reconquise par les rois catholiques en 1492. Installation et déjeuner à l hôtel. Visite guidée de l'Alhambra, célèbre forteresse arabe qui abrite l'Alcazaba, leurs dépendances et leurs patios, la Cour des Lions, le palais de Charles Quint",
	image : "http://www.lafrancaisedescircuits.biz/images/photo/B_257_ES_Grenade_165162905.jpg"

};



$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	for (var i = 0; i < menu.length; i++) {
		$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		}

	$('.navbar-brand').append(Titre);
	$('#SousTitre').append(SousTitre);
	$('#Description').append(Description);

	//Afficher le paragrpahe explicatif du circuit
	$('#Details').append(Details);

	//Afficher les listes
	$('#Liste1').append(Liste1);
	$('#Liste2').append(Liste2);
	$('#Liste3').append(Liste3);
	$('#Liste4').append(Liste4);

	//Afficher le programme
	$('#Programme').append(Programme);


	//Afficher le contenu du milieu du programme (image, titre et texte)
	$('#Jour1').append(Article1.title);
	$('#TexteJour1').append(Article1.texte);

	$('#Jour2').append(Article2.title);
	$('#TexteJour2').append(Article2.texte);

	$('#Jour3').append(Article3.title);
	$('#TexteJour3').append(Article3.texte);

	$('#Jour4').append(Article4.title);
	$('#TexteJour4').append(Article4.texte);
	
	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);

	//fonction cacher texte texte 1
	$('#TexteJour1').ready(function(){
		$('#TexteJour1').hide();
	});
	$('#Img1').click(function(){
		$('#TexteJour1').show();
	});

	//fonction cacher texte texte 2
	$('#TexteJour2').ready(function(){
		$('#TexteJour2').hide();
	});
	$('#Img2').click(function(){
		$('#TexteJour2').show();
	});
	
	//fonction cacher texte texte 3
	$('#TexteJour3').ready(function(){
		$('#TexteJour3').hide();
	});
	$('#Img3').click(function(){
		$('#TexteJour3').show();
	});

	//fonction cacher texte texte 4
	$('#TexteJour4').ready(function(){
		$('#TexteJour4').hide();
	});
	$('#Img4').click(function(){
		$('#TexteJour4').show();
	});

});


//fonction afficher grande image  cachée derrière image
	$('#GrandeImage2').ready(function(){
			$('#GrandeImage2').hide();
	});

	$('#GrandeImage2').mouseover(function(){
		$('#GrandeImage1').show();
		$("#GrandeImage2").hide();
	});

	 $('#GrandeImage1').mouseout(function(){
		$("#GrandeImage1").hide();
		$('#GrandeImage2').show();
	});



//fonction montrer l'image Copie derrière l'image 1 (passer curseur dessus)
	 $('#Img1Copie').mouseover(function(){
		$('#Img1').show();
		$("#Img1Copie").hide();
	});

	 $('#Img1').mouseout(function(){
		$("#Img1").hide();
		$('#Img1Copie').show();
	});

//fonction montrer l'image Copie derrière l'image 2 (passer curseur dessus)
	 $('#Img2Copie').mouseover(function(){
		$('#Img2').show();
		$("#Img2Copie").hide();
	});

	 $('#Img2').mouseout(function(){
		$("#Img2").hide();
		$('#Img2Copie').show();
	});


	 //fonction montrer l'image Copie derrière l'image 3 (passer curseur dessus)
	 $('#Img3Copie').mouseover(function(){
		$('#Img3').show();
		$("#Img3Copie").hide();
	});

	 $('#Img3').mouseout(function(){
		$("#Img3").hide();
		$('#Img3Copie').show();
	});


 //fonction montrer l'image Copie derrière l'image 4 (passer curseur dessus)
	 $('#Img4Copie').mouseover(function(){
		$('#Img4').show();
		$("#Img4Copie").hide();
	});

	 $('#Img4').mouseout(function(){
		$("#Img4").hide();
		$('#Img4Copie').show();
	});

  	