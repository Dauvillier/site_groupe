//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Circuits - Canada",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Circuits - Canada';

//Titre du circuit
var Description = 'Canada - Montréal';

//Détails du circuit
var Details = 'De la région de Québec en passant par Montréal et les Chutes du Niagara, venez découvrir les richesses culturelles de l Est du Canada, ainsi que son environnement naturel époustouflant. Un circuit au Canada extraordinaire qui vous laissera un souvenir impérissable. Survol des Chutes du Niagara en hélicoptère inclus. Montréal, Québec, le Lac St-Jean, Ottawa, Les Chutes du Niagara, Toronto De la région de Québec en passant par Montréal et les Chutes.';

//Liste des principaux thèmes du voyage
var Liste1 = 'Croisères';
var Liste2 = 'Paysages';
var Liste3 = 'Monuments';
var Liste4 = 'Architecture';

//Titre des articles
var Programme = 'Le programme';

//milieu les articles du programme 
var Article1 = {
	title : "Jour 1 - Toronto, Chutes du Niagara",
	texte : "Arrêt à Niagara-on-the-Lake, charmante petite ville située à l'embouchure de la rivière Niagara, sur le lac Ontario. Promenade le long du Niagara Parkway route panoramique longeant la rivière Niagara d'où vous aurez une magnifique vue sur l'ensemble de la vallée fruitière de Niagara. Déjeuner dans un restaurant avec vue panoramique sur les chutes.",
	image : "https://www.travelalerts.ca/wp-content/uploads/2017/10/niagara.jpg"

};

var Article2 = {
	title : "Jour 2 - Région de Montréal",
	texte : "Visite guidée de la métropole québécoise : le Mont-Royal, surnommé la Montagne par les Montréalais, le Stade Olympique, site des Jeux de 1976, avec sa haute tour inclinée (vue extérieure), la rue Ste-Catherine et son intense activité commerciale, le quartier du Vieux-Montréal, la cité souterraine. Repas. Après-midi libre pour une découverte personnelle de la ville. Repas nuit.",
	image : "http://stcathys.com/blog/wp-content/uploads/2017/05/Rooftop-Terrace.jpg"

};

var Article3 = {
	title : "Jour 3 - Ottawa, Région des Mille Îles",
	texte : "Départ pour Ottawa. A votre arrivée, tour d'orientation de la ville : le Château Laurier, les bâtiments officiels de la rue Wellington, le Parlement canadien et la Tour de la Paix, la Promenade Sussex, lieu de résidence officielle du Premier Ministre et du Gouverneur général ainsi que quelques ambassades, les grands musées nationaux, le quartier animé du marché By, sans oublier le canal Rideau.",
	image : "https://www.aeronetholidays.com/admin/package/17069Ottawa3.jpg"

};

var Article4 = {
	title : "Jour 4 - Tadoussac, Lac St Jean",
	texte : "Découverte de la région du lac St-Jean, véritable mer intérieure de 1350 km², dont les terres environnantes sont consacrées à l'agriculture. Arrêt au village de Ste-Rose-du-Nord connu pour son fjord, l'un des plus longs du globe. Visite du zoo de St-Félicien. Vous pourrez observer la faune canadienne dans un environnement naturel : loups, bisons, boeufs musqués peuvent être au rendez-vous.",
	image : "https://www.myatlas.xyz/gallery/500/59ece33f614b378ec85j.jpg?1"

};



$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	for (var i = 0; i < menu.length; i++) {
		$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		}

	$('.navbar-brand').append(Titre);
	$('#SousTitre').append(SousTitre);
	$('#Description').append(Description);

	//Afficher le paragrpahe explicatif du circuit
	$('#Details').append(Details);

	//Afficher les listes
	$('#Liste1').append(Liste1);
	$('#Liste2').append(Liste2);
	$('#Liste3').append(Liste3);
	$('#Liste4').append(Liste4);

	//Afficher le programme
	$('#Programme').append(Programme);


	//Afficher le contenu du milieu du programme (image, titre et texte)
	$('#Jour1').append(Article1.title);
	$('#TexteJour1').append(Article1.texte);

	$('#Jour2').append(Article2.title);
	$('#TexteJour2').append(Article2.texte);

	$('#Jour3').append(Article3.title);
	$('#TexteJour3').append(Article3.texte);

	$('#Jour4').append(Article4.title);
	$('#TexteJour4').append(Article4.texte);

	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);

//fonction cacher etxte
	$('#TexteJour1').ready(function(){
		$('#TexteJour1').hide();
	});
	$('#Img1').click(function(){
		$('#TexteJour1').show();
	});

	//fonction cacher texte texte 2
	$('#TexteJour2').ready(function(){
		$('#TexteJour2').hide();
	});
	$('#Img2').click(function(){
		$('#TexteJour2').show();
	});
	
	//fonction cacher texte texte 3
	$('#TexteJour3').ready(function(){
		$('#TexteJour3').hide();
	});
	$('#Img3').click(function(){
		$('#TexteJour3').show();
	});

	//fonction cacher texte texte 4
	$('#TexteJour4').ready(function(){
		$('#TexteJour4').hide();
	});
	$('#Img4').click(function(){
		$('#TexteJour4').show();
	});
	
//fonction afficher grande image  cachée derrière image
	$('#GrandeImage2').ready(function(){
			$('#GrandeImage2').hide();
	});

	$('#GrandeImage2').mouseover(function(){
		$('#GrandeImage1').show();
		$("#GrandeImage2").hide();
	});

	 $('#GrandeImage1').mouseout(function(){
		$("#GrandeImage1").hide();
		$('#GrandeImage2').show();
	});

});



//fonction montrer l'image Copie derrière l'image 1 (passer curseur dessus)
	 $('#Img1Copie').mouseover(function(){
		$('#Img1').show();
		$("#Img1Copie").hide();
	});

	 $('#Img1').mouseout(function(){
		$("#Img1").hide();
		$('#Img1Copie').show();
	});

//fonction montrer l'image Copie derrière l'image 2 (passer curseur dessus)
	 $('#Img2Copie').mouseover(function(){
		$('#Img2').show();
		$("#Img2Copie").hide();
	});

	 $('#Img2').mouseout(function(){
		$("#Img2").hide();
		$('#Img2Copie').show();
	});


	 //fonction montrer l'image Copie derrière l'image 3 (passer curseur dessus)
	 $('#Img3Copie').mouseover(function(){
		$('#Img3').show();
		$("#Img3Copie").hide();
	});

	 $('#Img3').mouseout(function(){
		$("#Img3").hide();
		$('#Img3Copie').show();
	});


 //fonction montrer l'image Copie derrière l'image 4 (passer curseur dessus)
	 $('#Img4Copie').mouseover(function(){
		$('#Img4').show();
		$("#Img4Copie").hide();
	});

	 $('#Img4').mouseout(function(){
		$("#Img4").hide();
		$('#Img4Copie').show();
	});

  	

