//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Circuits - Autriche",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Circuits - Autriche';

//Titre du circuit
var Description = 'Autriche - Grand Tour d Autriche';

//Détails du circuit
var Details = ' Cet itinéraire très complet vous fera découvrir les principales cités historiques du pays, comme Innsbruck, Salzbourg, Vienne et Graz. Immergez-vous dans le Vienne impérial, marchez sur les traces de Mozart et découvrez le folklore autrichien.';

//Liste des principaux thèmes du voyage
var Liste1 = 'Architecture';
var Liste2 = 'Lacs' ;
var Liste3 = 'Verdure';
var Liste4 = 'Paysages';

//Titre des articles
var Programme = 'Le programme';

//milieu les articles du programme 
var Article1 = {
	title : "Jour 1 - Vienne",
	texte : "Après un transfert en voiture (1 heure environ) vous pourrez découvrir le village du père Noel et aller a la rencontre du père Noel en personne! Après votre repas au restaurant, nous vous emmènerons visiter le musée de l’arctique a Rovaniemi ; les visites se font en autonomie.",
	image : "https://cdn.austria.info/media/17083/thumbnails/stadtansicht-wien--oesterreich-werbung-julius-silver--d.jpg.3146489.jpg"

};

var Article2 = {
	title : "Jour 2 - Salzbourg",
	texte : "Visite guidée d'Innsbruck, la capitale tyrolienne avec la vieille ville, son célèbre « Petit toit d'or » et ses belles maisons datant de l'époque de l'empereur Maximilien 1er. Entrée au Palais Impérial, témoin du pouvoir et de la richesses des règnes passés. Déjeuner. Montée en ascenseur au tremplin de saut à ski de Bergisel d'où l'on peut admirer un superbe panorama sur les Alpes. Continuation vers Salzbourg, située sur les rives de la Salzach. Dîner et nuit dans la région de Salzbourg.",
	image : "http://www.lafrancaisedescircuits.biz/images/photo/B_363_iStock_000063299705_web.jpg"

};

var Article3 = {
	title : "Jour 3 - Munich",
	texte : "Retrouvez votre guide dans le centre de Munich à 14:45 et montez à bord d’un autocar à destination d’une brasserie locale, qui produit l’une des bières allemandes les plus vendues et qui demeure un haut lieu de l’Oktoberfest. À votre arrivée, rencontrez des brasseurs expérimentés qui vous parleront de l’histoire de la brasserie et de la culture de la bière en Allemagne. Découvrez comment la bière est brassée suivant « l’Ordre de Pureté » bavarois d’origine, qui respecte le Décret de la pureté bavaroise instauré en 1516 par le duc Guillaume IV de Bavière.", 
	image : "https://www.jewish-holiday.com/wp-content/uploads/2014/09/FI-Muenchen.jpg"

};

var Article4 = {
	title : "Jour 4 - Grossglockner",
	texte : "Départ pour le Tyrol via la route panoramique du Großglockner (fermé jusqu'au 01/05 et selon les conditions météorologiques), point culminant des Alpes autrichiennes. De la Franz Josef Höhe, terrasse située à 2505 m, panorama extraordinaire sur le Großglockner qui culmine à 3798 mètres d'altitude et sur le plus vaste glacier d'Autriche : le Pasterze qui s'étend sur pas moins de neuf kilomètres. Déjeuner au restaurant offrant une vue exceptionnelle sur les massifs. Dîner et nuit au Tyrol.",
	image : "http://www.lafrancaisedescircuits.biz/images/photo/B_243_AT_HALLSTATT_111896819.jpg"

};



$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	for (var i = 0; i < menu.length; i++) {
		$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		}

	$('.navbar-brand').append(Titre);
	$('#SousTitre').append(SousTitre);
	$('#Description').append(Description);

	//Afficher le paragrpahe explicatif du circuit
	$('#Details').append(Details);

	//Afficher les listes
	$('#Liste1').append(Liste1);
	$('#Liste2').append(Liste2);
	$('#Liste3').append(Liste3);
	$('#Liste4').append(Liste4);

	//Afficher le programme
	$('#Programme').append(Programme);


	//Afficher le contenu du milieu du programme (image, titre et texte)
	$('#Jour1').append(Article1.title);
	$('#TexteJour1').append(Article1.texte);

	$('#Jour2').append(Article2.title);
	$('#TexteJour2').append(Article2.texte);

	$('#Jour3').append(Article3.title);
	$('#TexteJour3').append(Article3.texte);

	$('#Jour4').append(Article4.title);
	$('#TexteJour4').append(Article4.texte);
	
	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);



//fonction cacher texte texte 1
	$('#TexteJour1').ready(function(){
		$('#TexteJour1').hide();
	});
	$('#Img1').click(function(){
		$('#TexteJour1').show();
	});

	//fonction cacher texte texte 2
	$('#TexteJour2').ready(function(){
		$('#TexteJour2').hide();
	});
	$('#Img2').click(function(){
		$('#TexteJour2').show();
	});
	
	//fonction cacher texte texte 3
	$('#TexteJour3').ready(function(){
		$('#TexteJour3').hide();
	});
	$('#Img3').click(function(){
		$('#TexteJour3').show();
	});

	//fonction cacher texte texte 4
	$('#TexteJour4').ready(function(){
		$('#TexteJour4').hide();
	});
	$('#Img4').click(function(){
		$('#TexteJour4').show();
	});

});


//fonction afficher grande image  cachée derrière image
	$('#GrandeImage2').ready(function(){
			$('#GrandeImage2').hide();
	});

	$('#GrandeImage2').mouseover(function(){
		$('#GrandeImage1').show();
		$("#GrandeImage2").hide();
	});

	 $('#GrandeImage1').mouseout(function(){
		$("#GrandeImage1").hide();
		$('#GrandeImage2').show();
	});



//fonction montrer l'image Copie derrière l'image 1 (passer curseur dessus)
	 $('#Img1Copie').mouseover(function(){
		$('#Img1').show();
		$("#Img1Copie").hide();
	});

	 $('#Img1').mouseout(function(){
		$("#Img1").hide();
		$('#Img1Copie').show();
	});

//fonction montrer l'image Copie derrière l'image 2 (passer curseur dessus)
	 $('#Img2Copie').mouseover(function(){
		$('#Img2').show();
		$("#Img2Copie").hide();
	});

	 $('#Img2').mouseout(function(){
		$("#Img2").hide();
		$('#Img2Copie').show();
	});


	 //fonction montrer l'image Copie derrière l'image 3 (passer curseur dessus)
	 $('#Img3Copie').mouseover(function(){
		$('#Img3').show();
		$("#Img3Copie").hide();
	});

	 $('#Img3').mouseout(function(){
		$("#Img3").hide();
		$('#Img3Copie').show();
	});


 //fonction montrer l'image Copie derrière l'image 4 (passer curseur dessus)
	 $('#Img4Copie').mouseover(function(){
		$('#Img4').show();
		$("#Img4Copie").hide();
	});

	 $('#Img4').mouseout(function(){
		$("#Img4").hide();
		$('#Img4Copie').show();
	});