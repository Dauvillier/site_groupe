//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Circuits - Finlande",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Circuits - Finlande';

//Titre du circuit
var Description = 'Finlande - Laponie';

//Détails du circuit
var Details = 'Un séjour multi-activités, de 8 jours, très varié à faire en famille ou entre amis, qui ravira les petits et les grands. Au programme : chien de traîneau, motoneige, randonnée sur le parc naturel de Auttiköngas ou Korouoma, rencontre avec les rennes, et bien sûr, la visite du village du père Noel et du musée de l arctique et visite du zoo de Ranua.';

//Liste des principaux thèmes du voyage
var Liste1 = 'Paysages enneigés';
var Liste2 = 'Animaux' ;
var Liste3 = 'Montagnes';
var Liste4 = 'Châlets';

//Titre des articles
var Programme = 'Le programme';

//milieu les articles du programme 
var Article1 = {
	title : "Jour 1 - Laponie, Le village du Père Noël",
	texte : "Après un transfert en voiture (1 heure environ) vous pourrez découvrir le village du père Noel et aller a la rencontre du père Noel en personne! Après votre repas au restaurant, nous vous emmènerons visiter le musée de l’arctique a Rovaniemi ; les visites se font en autonomie.",
	image : "http://blog.hugavenue.com/wp-content/uploads/2015/12/noel-laponie.jpg"

};

var Article2 = {
	title : "Jour 2 - Initiation motoneige",
	texte : "Au cour de cette journée, vous serez initié à l’activité motoneige. Vous prenez possession du matériel et serez entraînés. Après le lunch, en petit groupe de 4 à 5 personnes, deux personnes par motoneige, vous partirez pour une randonnée à travers les forêts de Autti.",
	image : "https://i.pinimg.com/736x/06/1c/b9/061cb9403b5edd0b26027e2d22e1d678--snowmobiles-safari.jpg"

};

var Article3 = {
	title : "Jour 3 - Promenade avec les Rennes",
	texte : "Nous partons dès le matin en voiture, pour un safari photo. Tout d’abord, vous irez a la rencontre de l’élevage de rennes de Pajulampi ; vous rencontrerez les rennes, sur leur lieux d’élevage et pourrez même les nourrir vous même, et toucher les plus téméraire.",
	image : "http://www.voyages-finlande.com/imagefinlande/groupefinlande11.jpg"

};

var Article4 = {
	title : "Jour 4 - Promenade au bord du Lac Inari",
	texte : "Découverte du lac d'Inari qui est, de par sa taille (1 153 km2), le 2e du monde au nord du cercle polaire. Les monts qui l'entourent et ses 3 000 îles rocheuses peuplées de pins et de bouleaux se reflètent dans ses eaux sombres mais pures, qui ne dégèlent qu'à la mi-juin.",
	image : "https://exp.cdn-hotels.com/hotels/18000000/17520000/17519900/17519861/df62edec_y.jpg"

};



$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	for (var i = 0; i < menu.length; i++) {
		$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		}

	$('.navbar-brand').append(Titre);
	$('#SousTitre').append(SousTitre);
	$('#Description').append(Description);

	//Afficher le paragrpahe explicatif du circuit
	$('#Details').append(Details);

	//Afficher les listes
	$('#Liste1').append(Liste1);
	$('#Liste2').append(Liste2);
	$('#Liste3').append(Liste3);
	$('#Liste4').append(Liste4);

	//Afficher le programme
	$('#Programme').append(Programme);


	//Afficher le contenu du milieu du programme (image, titre et texte)
	$('#Jour1').append(Article1.title);
	$('#TexteJour1').append(Article1.texte);

	$('#Jour2').append(Article2.title);
	$('#TexteJour2').append(Article2.texte);

	$('#Jour3').append(Article3.title);
	$('#TexteJour3').append(Article3.texte);

	$('#Jour4').append(Article4.title);
	$('#TexteJour4').append(Article4.texte);
	
	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);


//fonction cacher texte texte 1
	$('#TexteJour1').ready(function(){
		$('#TexteJour1').hide();
	});
	$('#Img1').click(function(){
		$('#TexteJour1').show();
	});

	//fonction cacher texte texte 2
	$('#TexteJour2').ready(function(){
		$('#TexteJour2').hide();
	});
	$('#Img2').click(function(){
		$('#TexteJour2').show();
	});
	
	//fonction cacher texte texte 3
	$('#TexteJour3').ready(function(){
		$('#TexteJour3').hide();
	});
	$('#Img3').click(function(){
		$('#TexteJour3').show();
	});

	//fonction cacher texte texte 4
	$('#TexteJour4').ready(function(){
		$('#TexteJour4').hide();
	});
	$('#Img4').click(function(){
		$('#TexteJour4').show();
	});

});


//fonction afficher grande image  cachée derrière image
	$('#GrandeImage2').ready(function(){
			$('#GrandeImage2').hide();
	});

	$('#GrandeImage2').mouseover(function(){
		$('#GrandeImage1').show();
		$("#GrandeImage2").hide();
	});

	 $('#GrandeImage1').mouseout(function(){
		$("#GrandeImage1").hide();
		$('#GrandeImage2').show();
	});




//fonction montrer l'image Copie derrière l'image 1 (passer curseur dessus)
	 $('#Img1Copie').mouseover(function(){
		$('#Img1').show();
		$("#Img1Copie").hide();
	});

	 $('#Img1').mouseout(function(){
		$("#Img1").hide();
		$('#Img1Copie').show();
	});

//fonction montrer l'image Copie derrière l'image 2 (passer curseur dessus)
	 $('#Img2Copie').mouseover(function(){
		$('#Img2').show();
		$("#Img2Copie").hide();
	});

	 $('#Img2').mouseout(function(){
		$("#Img2").hide();
		$('#Img2Copie').show();
	});


	 //fonction montrer l'image Copie derrière l'image 3 (passer curseur dessus)
	 $('#Img3Copie').mouseover(function(){
		$('#Img3').show();
		$("#Img3Copie").hide();
	});

	 $('#Img3').mouseout(function(){
		$("#Img3").hide();
		$('#Img3Copie').show();
	});


 //fonction montrer l'image Copie derrière l'image 4 (passer curseur dessus)
	 $('#Img4Copie').mouseover(function(){
		$('#Img4').show();
		$("#Img4Copie").hide();
	});

	 $('#Img4').mouseout(function(){
		$("#Img4").hide();
		$('#Img4Copie').show();
	});