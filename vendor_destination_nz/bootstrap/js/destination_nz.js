//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Destinations - Nouvelle Zélande';

//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';

//Texte info souris effet
var TexteInfo = "Découvre en plus ..";
console.log(TexteInfo);

//Milieu
var Article1 = {
	title : "Huka Falls",
	texte : "Impossible de les manquer ! L'eau est si pure et la concentration en bulles d'oxygène si forte qu'elle nous apparaît bleu turquoise.",
	image : "http://www.indigo-voyages.fr/fs/nouvelle_zelande/normal/dc80d-chute_huka.jpg"

};

var Article2 = {
	title : "Parc National Waiotapu",
	texte : "Aussi appelé le Yellowstone de Nouvelle-Zélande, il est très spécial car il est situé à la rencontre de deux plaques tectoniques.",
	image : "http://www.happies.fr/wp-content/uploads/2016/05/paysages-13-300x200.jpg"

};

var Article3 = {
	title : "Grottes de Waitomo",
	texte : "Imaginez une roche de plus de 200 mètres d'épaisseur, haute comme un gratte-ciel.",
	image : "https://i.pinimg.com/originals/f5/15/eb/f515eb93afcc477ab96ee030fadb8a6c.jpg"

};


//afficher titre voyage
$('title').html(SiteConfig.title);
console.log(SiteConfig);

//footer
$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('.Destinations').append(menu[0]);
	$('#SousTitre').append(SousTitre);


	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton1);
	$('#Bouton2').append(Bouton2);

	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Huka').append(Article1.title);
	$('#TexteHuka').append(Article1.texte);

	$('#Wai').append(Article2.title);
	$('#TexteWai').append(Article2.texte);

	$('#Grotte').append(Article3.title);
	$('#TexteGrotte').append(Article3.texte);

	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);


	$('.img1').mouseov
	function bigImg(x) {
		x.style.height = "64px";
		x.style.width = "64px";
	}

	function normalImg(x) {
		x.style.height = "32px";
		x.style.width = "32px";
	}

//Effet flou passage souris
	/*$('#Flou').mouseout(function(){
	
			$('#Image1').hide();
			$('#Image2').show();
		});
		
	$('#Flou').mouseenter(function(){
		$('#Image1').show();
		$('#Image2').hide();
	});*/


	//Effet flou + fleche quand souris passe sur img
	$('#Effet').ready(function(){
		$('#Image2').mouseout(function(){
		$('#Image2').hide();
		$('#Image3').hide();
		return false;
		});

		$('#Image1').mouseenter(function(){
		$('#Image2').show();
		$('#Image3').show();
		return false;
		});

	});

	$('#Effet1').ready(function(){
		$('#Image22').mouseout(function(){
		$('#Image22').hide();
		$('#Image33').hide();
		return false;
		});

		$('#Image11').mouseenter(function(){
		$('#Image22').show();
		$('#Image33').show();
		return false;
		});

	});

	$('#Effet11').ready(function(){
		$('#Image222').mouseout(function(){
		$('#Image222').hide();
		$('#Image333').hide();
		return false;
		});

		$('#Image111').mouseenter(function(){
		$('#Image222').show();
		$('#Image333').show();
		return false;
		});

	});
	
});

