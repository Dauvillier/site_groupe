//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Destinations - Norvège';

//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';

//Milieu
var Article1 = {
	title : "Opéra d'Oslo",
	texte : "La nature norvégienne est libre d'accès pour tous et l'Opéra d'Oslo, inauguré en 2008, a été construit dans le même esprit.",
	image : "http://www.nortours.fr/wp-content/uploads/2017/11/1-c-Innovation-Norway-Tord-Baklund-VisitOslo-Winter-sun-at-the-opera-Oslo-300x200.jpg"

};

var Article2 = {
	title : "La ville de Stavanger",
	texte : "Paysages à couper le souffle avec de splendides fjords et montagnes, sans oublier les interminables plages de sable blanc. Ancienne capitale européenne de la culture, Stavanger dispose également d'un nombre impressionnant de musées où ont lieu d'innombrables événements culturels.",
	image : "https://img.ev.mu/images/articles/300x200/867283.jpg"

};

var Article3 = {
	title : "Urnes stavkirke",
	texte : "L’église d’Urnes a été construite dans les années 1130. Son architecture et son style décoratif sont uniques. Elle compte parmi les plus anciennes des églises de bois conservées, et témoigne d’une maîtrise artisanale et sculpturale exceptionnelles.",
	image : "http://media.paperblog.fr/i/733/7332197/voyage-norvege-6-nryfjord-eglises-bois-debout-L-fyTofm.jpeg"

};


//afficher titre voyage
$('title').html(SiteConfig.title);
console.log(SiteConfig);

//footer
$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('.Destinations').append(menu[0]);
	$('#SousTitre').append(SousTitre);

	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Opera').append(Article1.title);
	$('#TexteOpera').append(Article1.texte);

	$('#Stavanger').append(Article2.title);
	$('#TexteStavanger').append(Article2.texte);

	$('#Eglise').append(Article3.title);
	$('#TexteEglise').append(Article3.texte);

	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton1);
	$('#Bouton2').append(Bouton2);

	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);

	$('.img1').mouseov
	function bigImg(x) {
		x.style.height = "64px";
		x.style.width = "64px";
	}

	function normalImg(x) {
		x.style.height = "32px";
		x.style.width = "32px";
	}

//Effet flou + fleche quand souris passe sur img
	$('#Effet').ready(function(){
		$('#Image2').mouseout(function(){
		$('#Image2').hide();
		$('#Image3').hide();
		return false;
		});

		$('#Image1').mouseenter(function(){
		$('#Image2').show();
		$('#Image3').show();
		return false;
		});

	});

	$('#Effet1').ready(function(){
		$('#Image22').mouseout(function(){
		$('#Image22').hide();
		$('#Image33').hide();
		return false;
		});

		$('#Image11').mouseenter(function(){
		$('#Image22').show();
		$('#Image33').show();
		return false;
		});

	});
	
	$('#Effet11').ready(function(){
		$('#Image222').mouseout(function(){
		$('#Image222').hide();
		$('#Image333').hide();
		return false;
		});

		$('#Image111').mouseenter(function(){
		$('#Image222').show();
		$('#Image333').show();
		return false;
		});

	});
	
});

