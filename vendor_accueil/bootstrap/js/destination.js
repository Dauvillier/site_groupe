//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//footer + titre page voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header titre barre navigation
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil
var SousTitre = 'Destinations - Vietnam';


//Milieu
var Article1 = {
	title : "Mausolée de Minh Mang",
	texte : "Admirez l'Imperial Tomb de l'empereur Minh Mang de Hue ; un monument funéraire en l'honneur de l'empereur Minh Mang avec le style architectural chinois.",
	image : "https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Hue_Vietnam_Tomb-of-Emperor-Minh-Mang-01.jpg/300px-Hue_Vietnam_Tomb-of-Emperor-Minh-Mang-01.jpg"

};

var Article2 = {
	title : "Muong Hoa Valley",
	texte : "Venez découvrir la vallée de Muong Hoa Valley au nord du Vietnam (Sapa) et qui se trouve parmi les plus beaux paysages dans le monde.",
	image : "http://sapatrekkingtour.com/UserFiles/image/Sapa%20tours%20300x200/Muong%20Hoa%20valley.jpg"

};

var Article3 = {
	title : "Lady Buddha",
	texte : "Appréciez le marbre blanc de la statue Lady Buddha, mesurant 72 mètres de haut et se situant sur la péninsule Son Tra de Da Nang dominant la baie.",
	image : "http://goasiatravel.com/content/images/thumbs/0013747_marble-mountains-and-monkey-mountains-tour_300.jpeg"

};


//afficher titre voyage
$('title').html(SiteConfig.title);
console.log(SiteConfig);

//footer
$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	for (var i = 0; i < menu.length; i++) {
		$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		}

	$('.navbar-brand').append(Titre);
	$('.Destinations').append(menu[0]);
	$('#SousTitre').append(SousTitre);

	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Mausolee').append(Article1.title);
	$('#TexteMausolee').append(Article1.texte);

	$('#Muong').append(Article2.title);
	$('#TexteMuong').append(Article2.texte);

	$('#LadyBuddha').append(Article3.title);
	$('#TexteBuddha').append(Article3.texte);

	//Afficher footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author); //+ '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html('</a>-' + "'>" + SiteConfig.numero);
	$('#footer3').html("<a href= ' :" + "'>" + SiteConfig.insta);
	$('#footer4').html("<a href= ' :" + "'>" + SiteConfig.fb);
	$('#footer5').html("<a href= ' :" + "'>" + SiteConfig.twitter);

	
});

