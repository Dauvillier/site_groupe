//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//Footer + titre Page Voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};

//header Titre barre nav
var Titre = 'Les Petites Pastèques en vacances';

//header titre page accueil 
var SousTitre = 'Derniers voyages';

//Milieu
var Article1 = {
	title : "Thaïlande",
	texte : "Découvrez les ruines de l'ancienne capitale inscrite au patrimoine mondial de l'UNESCO ...",
	image : "https://lucileistravelling.files.wordpress.com/2017/05/cropped-pai-thailande1.jpg?w=200"

};

//Milieu
var Article2 = {
	title : "Vietnam",
	texte : "Visitez la cité impériale de Thang Long à Hano qui a servi de capitale du Vietnam pendant huit siècles !",
	image : "https://www.travel-associates.com.au/sites/default/files/product-images/INT_vietnam_halong_bay_4_INTREPID-06b560feb4e407643b308f55c8cbd886.jpg"

};

//Milieu
var Article3 ={
	title : "Nouvelle Zélande",
	texte : "Admirez les Kaoris de la forêt de Waipoua qui sont parmi les plus vieux et les plus gros arbres au monde ",
	image : "http://ekladata.com/4E49YJBrLJSGrXXtUuid--9I0h0@200x200.jpg"
};

//Milieu
var Article4 ={
	title : "Allemagne",
	texte :"Explorez les sombres canaux de Speicherstadt, littéralement “ville des entrepôts”",
	image : "https://www.bourse-des-vols.com/commun/images/001-villes/allemagne/hambourg/hambourg-hotel-de-ville-200x200.jpg"
};

//Milieu
var Article5 ={
	title : "Norvège",
	texte :" Allez voir le Trolltunga, c'est un incroyable rocher situé sur la localité d’Odda. Ce lieu est l’un des plus photographiés de Norvège !",
	image : "http://ekladata.com/_dtwZx9bVkj6m4HDMnDvzZGkqRw@200x200.jpg"
};

//Milieu
var Article6 ={
	title : "Grèce",
	texte : "Explorez les différentes îles de Grèce. Parmis celles-ci, l'île de Symi est très réputée pour ses éponges !",
	image :"https://i.pinimg.com/236x/43/36/9b/43369bb3065883b34d477edae19d4ec8--fira-santorini-santorini-island.jpg"
};

$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('.Actualites').append(menu[0]);
	$('#SousTitre').append(SousTitre);



	//Afficher le contenu du milieu (image, titre et texte)
	$('#Thailande').append(Article1.title);
	$('#TexteThailande').append(Article1.texte);

	$('#Vietnam').append(Article2.title);
	$('#TexteVietnam').append(Article2.texte);

	$('#NouvelleZelande').append(Article3.title);
	$('#TexteNouvelleZelande').append(Article3.texte);

	$('#Allemagne').append(Article4.title);
	$('#TexteAllemagne').append(Article4.texte);

	$('#Norvege').append(Article5.title);
	$('#TexteNorvege').append(Article5.texte);
	
	$('#Grece').append(Article6.title);
	$('#TexteGrece').append(Article6.texte);
	
	//footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author); //+ '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html('</a>-' + "'>" + SiteConfig.numero);
	$('#footer3').html("<a href= ' :" + "'>" + SiteConfig.insta);
	$('#footer4').html("<a href= ' :" + "'>" + SiteConfig.fb);
	$('#footer5').html("<a href= ' :" + "'>" + SiteConfig.twitter);

	//Effet flou passage souris
	/*$('#Flou').mouseover(function(){
		if{
			$('#Image').hide(1000);
			$('#Image2').show('slow');
		}
		else{
			$('#Image').show('slow');
			$('#Image2').hide(1000);
		}
		

	});*/
	//return true" onMouseOut="status=";return true"Lien

	$('index_accueil.body').ready(function(){
		$('#Image2').mouseout(function(){
		$('#Souris').hide();
		
		});

		$('#Image2').mouseenter(function(){
		$('body').cursor().hide();
		$('#Souris').show();
	
		});

		});

	//Tremblement texte au passage souris
	('#Tremblement').ready(function(){
		$('#Tremblement').mouseenter(function(){
		$('#Tremblement').fadeOut();
	});
	});

});
	