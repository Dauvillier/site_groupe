//header (barre navigation)
var menu = ['Actualités', 'Destinations', 'Circuits'];
console.log(menu);

//Footer + titre Page Voyages
var SiteConfig = {
	title : "Voyages",
	author : "Les ptites pastèques",
	contact : "LesPtitesPasteques@gmail.com",
	numero : "Tel : 0157649817",
	year : "2018",
	fb : "Facebook",
	twitter : "Twitter",
	insta : "Instagram",

	nbOfCategories : 8
};


//header Titre barre nav
var Titre = 'Les Petites Pastèques en vacances';

//Milieu
var Article1 = {
	title : "Norvège",
	texte : "La Norvège est une destination avec l’un des paysages les plus spectaculaires dans le monde, avec des montagnes plongeant dans la mer, les fjords, les aurores boréales et le soleil de minuit.",
	image : "https://www.noteauvoyageur.eu/wp-content/uploads/2014/07/galway001.jpg"

};

var Article2 = {
	title : "Nouvelle Zélande",
	texte : "Des paysages à couper le souffle qui ne ressemblent à aucun autre au monde. Ils sont d’une variété inégalée pour un pays de seulement 269 000 km2. Le reflet des montagnes enneigées dans les lacs, les fjords, les glaciers, les volcans, ...",
	image : "https://www.detournz.com/ic/2648391531/slideshow-circuit5-c-tongarirolandscape.jpg"

};

var Article3 = {
	title : "Thaïlande",
	texte : "La Thaïlande est un des pays les plus touristiques d’Asie. Non sans raison. C’est en outre une destination où il est facile de voyager en indépendant, en sac à dos. La Thaïlande est une des destinations les plus prisées des backpakers.",
	image : "http://www.atoursdumonde.com/katali/wp-content/uploads/2015/09/coron-900x-700x300.jpg"

};

var Article4 = {
	title : "Vietnam",
	texte : "Comme beaucoup d’autres pays voisins en Asie du Sud-Est, le Vietnam a un trésor précieux: ses habitants. Partout où vous allez, vous serez accueillis par des sourires et l’accuei agréable des locaux.",
	image : "https://i2.wp.com/antholagroup.com/sg/wp-content/uploads/2017/02/hanoi.jpg?resize=700%2C300&ssl=1"

};


//milieu bouton
var Bouton = 'Par ici !';
var Bouton1 = 'Par ici !';
var Bouton2 = 'Par ici !';


$(function(){

	$('title').html(SiteConfig.title);

	//header
	//$('#BarreNavigation').html(menu.Actualités);
	//for (var i = 0; i < menu.length; i++) {
		//$('#navbarResponsive').append("<li><a href='#'>" + menu[i] + "</a></li>");
		//}

	$('.navbar-brand').append(Titre);
	$('#Destinations').append(menu[1]);
	


	//afficher le footer
	$('#footer1').html("Copyright &copy; <a href= 'mail to :" + SiteConfig.contact + "'>" + SiteConfig.author + '</a>-' + "'>" + SiteConfig.numero);
	$('#footer2').html("<a href= ' :" + "'>" + SiteConfig.insta + "<a href= ' :" + "'>" + SiteConfig.fb + "<a href= ' :" + "'>" + SiteConfig.twitter);


	//Afficher le contenu du milieus ; les articles (image, titre et texte)
	$('#Norvege').append(Article1.title);
	$('#TexteNorvege').append(Article1.texte);

	$('#Nz').append(Article2.title);
	$('#TexteNz').append(Article2.texte);

	$('#Thailande').append(Article3.title);
	$('#TexteThailande').append(Article3.texte);

	$('#Vietnam').append(Article4.title);
	$('#TexteVietnam').append(Article4.texte);


	//Afficher texte bouton
	$('#Bouton').append(Bouton);
	$('#Bouton1').append(Bouton);
	$('#Bouton2').append(Bouton);
	$('#Bouton3').append(Bouton);

		//Effet bouton quans souris passe
	$('#Bouton').mouseenter(function(){
		$('#Bouton').fadeOut().fadeIn();
	});

	//Effet bouton quans souris passe
	$('#Bouton1').mouseenter(function(){
		$('#Bouton1').fadeOut().fadeIn();
	});

	//Effet bouton quans souris passe
	$('#Bouton2').mouseenter(function(){
		$('#Bouton2').fadeOut().fadeIn();
	});

	//Effet bouton quans souris passe
	$('#Bouton3').mouseenter(function(){
		$('#Bouton3').fadeOut().fadeIn();
	});

});